﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjetoGazin.Site.Anticorrupcao;
using ProjetoGazin.Site.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoGazin.Site.Controllers
{
    public class DevelopersController : Controller
    {
        public IActionResult Index()
        {
            var model = DevelopersClient.Get();

            return View(model);
        }

        [HttpGet]
        public IActionResult Buscar(string nome, int pagina)
        {
            if (pagina == 0)
                pagina = 1;

            var model = DevelopersClient.GetPorNomePaginado(nome, pagina);
            return View("_Lista", model);
        }

        [HttpGet]
        public IActionResult Cadastro()
        {
            var model = new DeveloperViewModel();
            return View("_Cadastro", model);
        }

        [HttpPost]
        public IActionResult ConfirmarCadastro(DeveloperViewModel model)
        {
            DevelopersClient.Post(model);
            return this.Index();
        }

        [HttpGet]
        public IActionResult Edicao(int id)
        {
            var model = DevelopersClient.GetPorId(id);
            return View("_Edicao", model);
        }

        [HttpPost]
        public IActionResult ConfirmarEdicao(DeveloperViewModel model)
        {
            DevelopersClient.Put(model);
            return this.Index();
        }

        [HttpDelete]
        public IActionResult Excluir(int id)
        {
            DevelopersClient.Delete(id);
            return this.Index();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
