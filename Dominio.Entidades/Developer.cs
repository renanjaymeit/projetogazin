﻿using System;

namespace ProjetoGazin.Dominio.Entidades
{
    public class Developer : Identificador
    {
        public string Nome { get; set; }
        public string Hobby { get; set; }
        public char Sexo { get; set; }
        public int Idade { get; set; }
        public DateTime DataNascimento { get; set; }
    }
}
