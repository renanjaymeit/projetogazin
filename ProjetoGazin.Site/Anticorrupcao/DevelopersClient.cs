﻿using ProjetoGazin.Site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoGazin.Site.Anticorrupcao
{
    public class DevelopersClient
    {
        private static readonly string rota = "/developers";

        public static List<DeveloperViewModel> Get()
        {
            var client = new Client(rota);

            return (client.Get<List<DeveloperViewModel>>());
        }
        
        public static List<DeveloperViewModel> GetPorNomePaginado(string nome, int pagina)
        {
            var client = new Client(string.Format("{0}/{1}/{2}", rota, nome, pagina.ToString()));

            return (client.Get<List<DeveloperViewModel>>());
        }

        public static DeveloperViewModel GetPorId(int id)
        {
            var client = new Client(string.Format("{0}/{1}", rota, id.ToString()));

            return (client.Get<DeveloperViewModel>());
        }
        
        public static DeveloperViewModel Post(DeveloperViewModel model)
        {
            var client = new Client(rota);

            return client.Post<DeveloperViewModel>(model);
        }

        public static DeveloperViewModel Put(DeveloperViewModel model)
        {
            var client = new Client(rota);

            return client.Put<DeveloperViewModel>(model);
        }

        public static void Delete(int id)
        {
            var client = new Client(string.Format("{0}/{1}", rota, id.ToString()));

            client.Delete();
        }
    }
}
