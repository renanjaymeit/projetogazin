﻿using System;

namespace ProjetoGazin.API.Models
{
    public class DeveloperModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Hobby { get; set; }
        public char Sexo { get; set; }
        public int Idade { get; set; }
        public DateTime DataNascimento { get; set; }
    }
}
