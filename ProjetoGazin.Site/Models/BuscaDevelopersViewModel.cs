using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjetoGazin.Site.Models
{
    public class BuscaDevelopersViewModel
    {
        [Display(Name = "Digite o nome de um developer para buscar")]
        public string ParametroBusca { get; set; }
        
        public List<DeveloperViewModel> ListaDevelopers { get; set; }

        public BuscaDevelopersViewModel()
        {
            this.ListaDevelopers = new List<DeveloperViewModel>();
        }
    }
}
