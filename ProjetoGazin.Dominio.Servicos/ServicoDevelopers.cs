﻿using ProjetoGazin.Dominio.Entidades;
using ProjetoGazin.Interfaces.Dominio.Servicos;
using ProjetoGazin.Interfaces.Infraestrutura.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjetoGazin.Dominio.Servicos
{
    public class ServicoDevelopers : ServicoBase<Developer>, IServicoDevelopers
    {
        private readonly IRepositorioDevelopers repositorioDevelopers;
        private readonly int qntPorPagina = 5;
        public ServicoDevelopers(IRepositorioDevelopers repositorio)
            :base(repositorio)
        {
            repositorioDevelopers = repositorio;
        }

        public IList<Developer> RecuperarPaginadoPorNome(string nome, int pagina)
        {
            try
            {
                return repositorioDevelopers.Select(nome)
                    .Skip((pagina - 1) * qntPorPagina)
                    .Take(qntPorPagina)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao realizar recuperação por nome paginada. Erro:" + ex.Message);
            }
        }
    }
}
