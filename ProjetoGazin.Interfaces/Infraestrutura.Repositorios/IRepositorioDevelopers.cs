﻿using ProjetoGazin.Dominio.Entidades;
using System;
using System.Collections.Generic;

namespace ProjetoGazin.Interfaces.Infraestrutura.Repositorios
{
    public interface IRepositorioDevelopers : IRepositorioBase<Developer>
    {
        IList<Developer> Select(string nome);
    }
}
