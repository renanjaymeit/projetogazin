﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjetoGazin.Dominio.Entidades;
using System;

namespace ProjetoGazin.Infraestrutura.Dados.Mapping
{
    public class DeveloperMap : IEntityTypeConfiguration<Developer>
    {
        public void Configure(EntityTypeBuilder<Developer> builder)
        {
            builder.ToTable("Developers");

            builder.HasKey(prop => prop.Id);

            builder.Property(prop => prop.Nome)
                .HasConversion(prop => prop.ToString(), prop => prop)
                .IsRequired()
                .HasColumnName("Nome")
                .HasColumnType("varchar(150)");

            builder.Property(prop => prop.Hobby)
               .HasConversion(prop => prop.ToString(), prop => prop)
               .HasColumnName("Hobby")
               .HasColumnType("varchar(500)");

            builder.Property(prop => prop.Sexo)
               .IsRequired()
               .HasColumnName("Sexo")
               .HasColumnType("char(1)");

            builder.Property(prop => prop.Idade)
               .IsRequired()
               .HasColumnName("Idade")
               .HasColumnType("int");

            builder.Property(prop => prop.DataNascimento)
               .IsRequired()
               .HasColumnName("DataNascimento")
               .HasColumnType("Date");
        }
    }
}
