using System;

namespace ProjetoGazin.Site.Models
{
    public class OpcaoSexoViewModel
    {
        public string Valor { get; set; }
        public string Descricao { get; set; }
    }
}
