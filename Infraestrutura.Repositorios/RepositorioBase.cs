﻿using ProjetoGazin.Dominio.Entidades;
using ProjetoGazin.Infraestrutura.Dados;
using ProjetoGazin.Interfaces.Infraestrutura.Dados;
using ProjetoGazin.Interfaces.Infraestrutura.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjetoGazin.Infraestrutura.Repositorios
{
    public class RepositorioBase<TEntity> : IRepositorioBase<TEntity> 
        where TEntity : Identificador
    {
        public RepositorioBase()
        {}

        public void Insert(TEntity obj)
        {
            using (var contexto = new Context())
            {
                contexto.Set<TEntity>().Add(obj);
                contexto.SaveChanges();
            }
        }

        public void Update(TEntity obj)
        {
            using (var contexto = new Context())
            {
                contexto.Entry(obj).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                contexto.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var contexto = new Context())
            {
                contexto.Set<TEntity>().Remove(Select(id));
                contexto.SaveChanges();
            }
        }

        public IList<TEntity> Select()
        {
            using (var contexto = new Context())
            {
                return contexto.Set<TEntity>().ToList();
            }
        }
        
        public TEntity Select(int id)
        {
            using (var contexto = new Context())
            {
                return contexto.Set<TEntity>().Find(id);
            }
        }
    }
}
