﻿using FluentValidation;
using System;
using System.Collections.Generic;

namespace ProjetoGazin.Interfaces.Dominio.Servicos
{
    public interface IServicoBase<TEntity>
    {
        public TEntity Incluir<TValidator>(TEntity obj)
            where TValidator : AbstractValidator<TEntity>;

        public void Alterar<TValidator>(TEntity obj)
            where TValidator : AbstractValidator<TEntity>;

        public void Excluir(int id);

        public IList<TEntity> Recuperar();

        public TEntity RecuperarPorId(int id);
    }
}
