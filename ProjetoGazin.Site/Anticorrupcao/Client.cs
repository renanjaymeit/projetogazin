﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoGazin.Site.Anticorrupcao
{
    public class Client
    {
        protected readonly string route;
        protected readonly string endpoint;
        protected readonly string urlApi = "http://localhost:57606/api";
        private readonly IConfigurationRoot configurationRoot;

        public Client(string route)
        {
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            configurationRoot = configurationBuilder.Build();

            var configUrl = configurationRoot.GetSection("ConfigUrls").GetChildren().FirstOrDefault(c => c.Key == "UrlApi");

            if (configUrl != null)
            {
                if (configUrl.Value.EndsWith("/"))
                    this.urlApi = configUrl.Value.Remove(configUrl.Value.Length - 1);
                else
                    this.urlApi = configUrl.Value;
            }

            this.endpoint = urlApi + route;
        }
        public T Get<T>()
        {
            using (var httpClient = new HttpClient())
            {
                using (var response = httpClient.GetAsync(this.endpoint).Result)
                {
                    if (response.IsSuccessStatusCode)
                        return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                    else
                        throw new Exception(response.Content.ReadAsStringAsync().Result);
                }
            }
        }

        public T Post<T>(T data)
        {
            using (var httpClient = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

                using (var response = httpClient.PostAsync(this.endpoint, content).Result)
                { 
                    if (response.IsSuccessStatusCode)
                        return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                    else
                        throw new Exception(response.Content.ReadAsStringAsync().Result);
                }
            }
        }

        public T Put<T>(T data)
        {
            using (var httpClient = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
                
                using (var response = httpClient.PutAsync(this.endpoint, content).Result)
                {
                    if (response.IsSuccessStatusCode)
                        return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                    else
                        throw new Exception(response.Content.ReadAsStringAsync().Result);
                }
            }
        }

        public void Delete()
        {
            using (var httpClient = new HttpClient())
            {
                using (var response = httpClient.DeleteAsync(this.endpoint).Result)
                {
                    if (!response.IsSuccessStatusCode)
                        throw new Exception(response.Content.ReadAsStringAsync().Result);
                }
            }
        }
    }
}


