﻿#region 
/*
GET/developers
Codes 200

GET/developers?
Codes 200 / 404

GET/developers/{id}
Codes 200 / 404

POST/developers
Codes 201 / 400

PUT/developers/{id}
Codes 200 / 400

DELETE/developers/{id}
Codes 204 / 400
*/
#endregion

using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjetoGazin.API.Models;
using ProjetoGazin.Dominio.Entidades;
using ProjetoGazin.Dominio.Validators;
using ProjetoGazin.Interfaces.Dominio.Servicos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoGazin.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DevelopersController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IServicoDevelopers servicoDevelopers;

        public DevelopersController(IMapper mapper,
            IServicoDevelopers servicoDevelopers)
        {
            this.mapper = mapper;
            this.servicoDevelopers = servicoDevelopers;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Execute(() => mapper.Map<List<DeveloperModel>>(servicoDevelopers.Recuperar()));
        }

        [HttpGet("{id}")]
        public IActionResult GetPorId(int id)
        {
            if (id == 0)
                return NotFound();

            return Execute(() => mapper.Map<DeveloperModel>(servicoDevelopers.RecuperarPorId(id)));
        }

        [HttpGet("{nome}/{pagina}")]
        public IActionResult GetPorNomePaginado(string nome, int pagina)
        {
            if (string.IsNullOrEmpty(nome) || pagina == 0)
                return NotFound();

            return Execute(() => mapper.Map<List<DeveloperModel>>(servicoDevelopers.RecuperarPaginadoPorNome(nome, pagina)));
        }

        [HttpPost]
        public IActionResult Post([FromBody] DeveloperModel model)
        {
            if (model == null)
                return NotFound();

            return Execute(() => mapper.Map<DeveloperModel>(servicoDevelopers.Incluir<ValidatorDevelopers>(mapper.Map<Developer>(model))));
        }

        [HttpPut]
        public IActionResult Put([FromBody] DeveloperModel model)
        {
            if (model == null)
                return NotFound();

            Execute(() =>
            {
                servicoDevelopers.Alterar<ValidatorDevelopers>(mapper.Map<Developer>(model));
                return true;
            });

            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id == 0)
                return NotFound();

            Execute(() =>
            {
                servicoDevelopers.Excluir(id);
                return true;
            });

            return new NoContentResult();
        }

        private IActionResult Execute(Func<object> func)
        {
            try
            {
                var result = func();

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
