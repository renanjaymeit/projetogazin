﻿using ProjetoGazin.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoGazin.Interfaces.Infraestrutura.Repositorios
{
    public interface IRepositorioBase<TEntity>
        where TEntity : Identificador
    {
        void Insert(TEntity obj);
        void Update(TEntity obj);
        void Delete(int id);
        IList<TEntity> Select();
        TEntity Select(int id);
    }
}
