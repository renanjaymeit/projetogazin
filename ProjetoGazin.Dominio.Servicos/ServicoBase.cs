﻿using FluentValidation;
using ProjetoGazin.Dominio.Entidades;
using ProjetoGazin.Interfaces.Dominio.Servicos;
using ProjetoGazin.Interfaces.Infraestrutura.Repositorios;
using System;
using System.Collections.Generic;

namespace ProjetoGazin.Dominio.Servicos
{
    public class ServicoBase<TEntity> : IServicoBase<TEntity> where TEntity : Identificador
    {
        protected readonly IRepositorioBase<TEntity> repositorio;

        public ServicoBase(IRepositorioBase<TEntity> repositorio)
        {
            this.repositorio = repositorio;
        }

        public TEntity Incluir<TValidator>(TEntity obj) 
            where TValidator : AbstractValidator<TEntity>
        {
            Validar(obj, Activator.CreateInstance<TValidator>());
            repositorio.Insert(obj);
            return obj;
        }

        public void Alterar<TValidator>(TEntity obj)
            where TValidator : AbstractValidator<TEntity>
        {
            Validar(obj, Activator.CreateInstance<TValidator>());
            repositorio.Update(obj);
        }

        public void Excluir(int id) => repositorio.Delete(id);

        public IList<TEntity> Recuperar() => repositorio.Select();

        public TEntity RecuperarPorId(int id) => repositorio.Select(id);

        private void Validar(TEntity obj, AbstractValidator<TEntity> validator)
        {
            if (obj == null)
                throw new Exception("Parâmetros insuficientes.");

            validator.ValidateAndThrow(obj);
        }
    }
}
