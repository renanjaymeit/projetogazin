# ProjetoGazin

Primeiro algumas considerações sobre o resultado final do projeto
Não consegui a tempo fazer os testes unitários, pois não tenho experiência com tal e acabei não conseguindo em tempo.
Nas telas apenas funcionam a listagem e o cadastro de um novo, também quis utilizar um framework novo pra mim aqui, mas acabei não conseguindo resolver uns bugs a tempo, e nem as validações em tela de tipo de campo etc.
A API em si está finalizada.

# Script de Banco - SQL Server

Tem um arquivo dentro da pasta SCRIPT_BANCO do projeto, que deve ser executado no SQL Server para criação do banco, da tabela e do usuário que o sistema vai utilizar para acessar a tabela

# Configurações API

Deve ser alterado no arquivo appsettings.json o valor da configuração ConnectionStrings - DefaultConnection para que seja a conexão com o servidor e base de banco do SQL Server de onde irá rodar

{
  "ConnectionStrings": {
    "DefaultConnection": "Server=localhost\\SQLEXPRESSR;Database=;User ID=gazin_usr;Password=pgusr;"
  }
}

# Configurações Site

Deve ser alterado no arquivo appsettings.json o valor da configuração ConfigUrls - UrlApi para o link de conexão com a API

{
  "ConfigUrls": {
    "UrlApi": "http://localhost:57606/api"
  }
}
