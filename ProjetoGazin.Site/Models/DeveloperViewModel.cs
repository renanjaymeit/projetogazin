using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjetoGazin.Site.Models
{
    public class DeveloperViewModel : IdentificadorViewModel
    {      
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Display(Name = "Sexo")]
        public char Sexo { get; set; }
        
        [Display(Name = "Hobby")]
        public string Hobby { get; set; }
        
        [Display(Name = "Idade")]
        public int Idade { get; set; }
        
        [Display(Name = "Data de Nascimento")]
        public DateTime DataNascimento { get; set; }
        
        public List<OpcaoSexoViewModel> OpcoesSexo { get; set; }

        public DeveloperViewModel()
        {
            this.OpcoesSexo = new List<OpcaoSexoViewModel>
            {
                new OpcaoSexoViewModel { Valor = "M", Descricao = "Masculino" },
                new OpcaoSexoViewModel { Valor = "F", Descricao = "Feminino" },
            };
        }
    }
}
