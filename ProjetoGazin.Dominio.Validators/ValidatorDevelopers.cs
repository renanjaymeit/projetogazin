﻿using FluentValidation;
using ProjetoGazin.Dominio.Entidades;
using System;

namespace ProjetoGazin.Dominio.Validators
{
    public class ValidatorDevelopers : AbstractValidator<Developer>
    {
        public ValidatorDevelopers()
        {
            RuleFor(d => d.Nome)
                .NotEmpty().WithMessage("Por favor informe um nome.")
                .NotNull().WithMessage("Por favor informe um nome.")
                .MaximumLength(150).WithMessage("Por favor, informe um nome com no máximo 150 caracteres.");

            RuleFor(d => d.Idade)
                .NotEmpty().WithMessage("Por favor informe uma idade.")
                .NotNull().WithMessage("Por favor informe uma idade.")
                .When(IdadeValida).WithMessage("Idade não corresponde com a data de nascimento informada.");

            RuleFor(d => d.Sexo)
                .NotEmpty().WithMessage("Por favor informe o sexo.")
                .NotNull().WithMessage("Por favor informe o sexo.")
                .Must(SexoValido).WithMessage("Sexo informado não é válido.");

            RuleFor(d => d.Hobby)
                .NotEmpty().WithMessage("Por favor informe um hobby.")
                .NotNull().WithMessage("Por favor informe um hobby.")
                .MaximumLength(500).WithMessage("Por favor, informe um hobby com no máximo 500 caracteres.");

            RuleFor(d => d.DataNascimento)
                .NotEmpty().WithMessage("Por favor informe uma data de nascimento.")
                .NotNull().WithMessage("Por favor informe uma data de nascimento.");
                        
            static bool SexoValido(char sexo) => (sexo == 'M' || sexo == 'F');

            static bool IdadeValida(Developer developer) 
            {
                var idade = DateTime.Now.Year - developer.DataNascimento.Year;
                idade = DateTime.Now.DayOfYear < developer.DataNascimento.DayOfYear ? idade - 1 : idade;
                return developer.Idade != idade;
            };
        }
    }
}
