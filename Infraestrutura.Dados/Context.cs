﻿using Microsoft.EntityFrameworkCore;
using ProjetoGazin.Dominio.Entidades;
using ProjetoGazin.Infraestrutura.Dados.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace ProjetoGazin.Infraestrutura.Dados
{  
    public class Context : DbContext
    {
        //private readonly ConfigConexao configConexao;

        //private string nomeStringDeConexao = "stringConexaoProjetoGazin";        
        private readonly IConfigurationRoot configurationRoot;

        public DbSet<Developer> Developers { get; set; }

        /*public Context(DbContextOptions<Context> options)
            : base(options)
        { }*/

        public Context()
            : base()
        {
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            configurationRoot = configurationBuilder.Build();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings[nomeStringDeConexao].ConnectionString);
            //optionsBuilder.UseSqlServer(configConexao.StringConexao);
            optionsBuilder.UseSqlServer(configurationRoot.GetConnectionString("DefaultConnection"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Developer>(new DeveloperMap().Configure);
        }
    }
}

