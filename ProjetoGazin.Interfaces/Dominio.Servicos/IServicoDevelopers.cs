﻿using ProjetoGazin.Dominio.Entidades;
using System;
using System.Collections.Generic;

namespace ProjetoGazin.Interfaces.Dominio.Servicos
{
    public interface IServicoDevelopers : IServicoBase<Developer>
    {
        IList<Developer> RecuperarPaginadoPorNome(string nome, int pagina);
    }
}
