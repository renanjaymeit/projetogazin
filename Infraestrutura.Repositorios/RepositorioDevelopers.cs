﻿using ProjetoGazin.Dominio.Entidades;
using ProjetoGazin.Infraestrutura.Dados;
using ProjetoGazin.Interfaces.Infraestrutura.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjetoGazin.Infraestrutura.Repositorios
{
    public class RepositorioDevelopers : RepositorioBase<Developer>, IRepositorioDevelopers
    {
        public IList<Developer> Select(string nome)
        {
            using (var contexto = new Context())
            {
                return contexto.Set<Developer>().Where(d => d.Nome.Contains(nome)).ToList();
            }
        }
    }
}
