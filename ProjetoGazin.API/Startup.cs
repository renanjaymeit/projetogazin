using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ProjetoGazin.Dominio.Entidades;
using ProjetoGazin.Dominio.Servicos;
using ProjetoGazin.Infraestrutura.Dados;
using ProjetoGazin.Infraestrutura.Repositorios;
using ProjetoGazin.Interfaces.Dominio.Servicos;
using ProjetoGazin.Interfaces.Infraestrutura.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoGazin.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //var configConexao = Configuration.GetSection("ConfigConexao");
            //services.AddDbContext<Context>(options => options.UseSqlServer(configConexao.Value));

            services.AddScoped<IServicoDevelopers, ServicoDevelopers>();
            services.AddScoped<IRepositorioDevelopers, RepositorioDevelopers>();

            services.AddAutoMapper(typeof(Startup));

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
