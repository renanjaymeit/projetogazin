CREATE DATABASE PROJETOGAZIN
GO

USE PROJETOGAZIN
GO

CREATE LOGIN gazin_usr WITH PASSWORD = 'pgusr'
GO

CREATE USER [gazin_usr] FOR LOGIN [gazin_usr]
EXEC sp_addrolemember N'db_owner', N'gazin_usr'
GO

ALTER LOGIN [gazin_usr] WITH DEFAULT_DATABASE = [PROJETOGAZIN]
GO

CREATE TABLE DBO.Developers(
    Id int identity,
    Nome varchar(150),
    Sexo char,
    Idade int,
    Hobby varchar(500),
    DataNascimento date,
    CONSTRAINT PK_Developers_Id PRIMARY KEY CLUSTERED (Id)
)
GO

INSERT INTO Developers(Nome, Sexo, Idade, Hobby, DataNascimento)
VALUES ('Jorge Basin Goes', 'M', 31, 'Caminhar a luz do luar', '10/02/1990')
GO

INSERT INTO Developers(Nome, Sexo, Idade, Hobby, DataNascimento)
VALUES ('José de Oliveira Pereira', 'M', 48, 'Pescar', '01/09/1973')
GO

INSERT INTO Developers(Nome, Sexo, Idade, Hobby, DataNascimento)
VALUES ('Andreia Lucimara da Silva', 'F', 25, 'Andar de bicicleta', '20/03/1996')
GO

INSERT INTO Developers(Nome, Sexo, Idade, Hobby, DataNascimento)
VALUES ('Luciana Alvarez Pereira', 'F', 36, 'Pular de paraquedas', '05/12/1984')
GO

INSERT INTO Developers(Nome, Sexo, Idade, Hobby, DataNascimento)
VALUES ('Pedro Marcio Pedroso', 'M', 31, 'Praticar escalada', '21/04/1990')
GO

INSERT INTO Developers(Nome, Sexo, Idade, Hobby, DataNascimento)
VALUES ('Mario Mariano da Costa', 'M', 24, 'Nadar no mar', '10/11/1996')
GO

INSERT INTO Developers(Nome, Sexo, Idade, Hobby, DataNascimento)
VALUES ('Juliana da Silva Santos', 'F', 22, 'Andar de skate', '02/02/1999')
GO